<?php

class Forum_model extends Core_Model
{
    public function __construct()
    {
        self::$table            = 'thread';
        self::$primaryKey       = 'id_thread';
    }

    public function allWithComment($number,$offset)
    {
        $this->db->order_by(self::$primaryKey,'DESC');
        $data  =    $this->db->get(self::$table,$number,$offset)->result();
            
        $counter = 0;
        foreach($data as $item)
        {
            $get = $this->db->where(self::$primaryKey,$item->id_thread)->get('komentar')->result();
            $data[$counter]->komentar = count($get);

            $counter++;
        }

        return $data;
    }

    public function searchWithComment($search,$key,$number,$offset)
    {
        $this->db->order_by(self::$primaryKey,'DESC');
        if ($key) {
                    $this->db->like($search,$key);
        }
        $data    =  $this->db->get(self::$table,$number,$offset)->result();
            
        $counter = 0;
        foreach($data as $item)
        {
            $get =  $this->db->where(self::$primaryKey,$item->id_thread)->get('komentar')->result();
            $data[$counter]->komentar = count($get);

            $counter++;
        }

        return $data;
    }

    public function countComment($id){
        $get = $this->db->where(self::$primaryKey,$id)->get('komentar')->result();
        $data = count($get);

        return $data; 

    }

    public function showComment($id){
        $query = $this->db->where(self::$primaryKey,$id)
                          ->get('komentar')
                          ->result();

        return $query;

    }

    public function create_comment($data){
        $data  = $this->db->insert( 'komentar' , $data );

        return $data;
    }

    public function thread_newest(){
        $this->db->order_by(self::$primaryKey,'DESC');
        $data  = $this->db->limit(4)
                          ->get(self::$table)
                          ->result();

        return $data;            
    }


}