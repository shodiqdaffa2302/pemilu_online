<?php

class Pemilih_model extends Core_Model
{
    public function __construct()
    {
        self::$table            = 'pemilih';
        self::$primaryKey       = 'nis';
    }

    public function checkLogin($data = array())
    {
        $check = $this->db->where(self::$primaryKey , $data['nis'])
                          ->get( self::$table )
                          ->row_array();

        if(!count($check))
        {
            return FALSE;
        }

        $checkPass = (password_verify($data['password'], $check['password']) );

        if($checkPass)
        {
            return $check;
        }
        else{
            return FALSE;
        }

    }
}