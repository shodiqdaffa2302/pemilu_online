<?php

class Calon_model extends Core_Model
{
    public function __construct()
    {
        self::$table            = 'calon';
        self::$primaryKey       = 'id';
    }
	
	public function destroyAll()
	{
		$this->db->empty_table('calon');
		$this->db->empty_table('paslon');
		$this->db->empty_table('pivot_paslon_calon');
		
	}
	
	public function tampil_pemilih($id){
		$data                              = $this->db->get_where( 'pemilih' , array('nis'=> $id));
		if ($data->num_rows() > 0) {
			return $data->row_array();
		}
        return false; 
	}

	public function createPaslon($data = array())
	{
		$table = 'paslon';
		$this->db->insert($table, $data);
		$getData = $this->db->get_where($table, $data)->row();
		return $getData;
	}
	
	public function createCalonPaslon($data = array(), $paslon_id)
	{
		$this->db->insert( self::$table, $data );
		
		$getData = $this->db->get_where(self::$table , $data)->row();
		$this->db->insert('pivot_paslon_calon',[
			'id_calon' => $getData->id,
			'id_paslon' => $paslon_id
		]);
	}
	
	public function updateCalonPaslon($data = array(), $paslon_id, $status)
	{


		$getData = $this->db->select('id_calon')
							->from('pivot_paslon_calon')
							->join('calon','calon.id = pivot_paslon_calon.id_calon')
							->where('calon.status', $status)
							->where('pivot_paslon_calon.id_paslon',$paslon_id)
							->get()
							->row();
		// var_dump($getData); 

		return		$this->db->set($data)
				 			 ->where('id',$getData->id_calon)
				 			 ->update('calon');

		// print_r($status);	
	}
	public function jumlah_suara(){
		return $query = $this->db->select_sum('jumlah_suara')
								 ->from('paslon')
								 ->get()
								 ->row();
	}

	public function destroyPaslon($paslon_id)
	{	
		$pivot_calon_id = $this->db->get_where('pivot_paslon_calon',[
			'id_paslon' => $paslon_id
		])->result();
	
		$this->db->delete('pivot_paslon_calon',[
			'id_paslon' => $paslon_id 
		]);
		
		foreach($pivot_calon_id as $calon_id)
		{
			$this->db->delete('calon',[
				'id'  => $calon_id->id_calon
			]);	
		}
		
		return	$this->db->delete('paslon',[
			'id' => $paslon_id
		]);
	}
	
	public function updatePaslon($data = [], $paslon_id)
	{
		// update paslon
		return $this->db->set([
			'visi' => $data['visi'],
			'misi' => $data['misi'],
			'warna' => $data['warna']
		])->where('id', $paslon_id)->update('paslon');
		
		
	}
	
	public function getPaslonWithCalon($paslon_id)
	{
		$data 		= $this->db->where('id', $paslon_id)
							   ->get('paslon')
							   ->row();
							   
		$pivot 		= $this->db->where('id_paslon', $paslon_id)
							   ->join('calon', 'pivot_paslon_calon.id_calon = calon.id')
							   ->get('pivot_paslon_calon')
							   ->result();
		
		$data->calon 	= $pivot;
		
					 
		return $data;
	}

	public function paslon()
	{
	    $data =	$this->db->get('paslon')->result();
		// $this->db->from( self::$table );
		// $this->db->join( self::$table.'.'.self::$primaryKey.' = pivot_paslon_calon.id_calon' );
		// $this->db->join( "pivot_paslon_calon.id_paslon = paslon.id" );
	
		$counter = 0;
		foreach ($data as $paslon) {
			$result = $this->db->select('*')
							   ->from('pivot_paslon_calon')
							   ->where('id_paslon',$paslon->id)
							   ->join(self::$table ,'pivot_paslon_calon.id_calon = '.self::$table.'.'.self::$primaryKey)
							   ->get()
							   ->result();
			
			$data[$counter]->calon = $result;
			$counter++;
		}
		
		// print_r($data); die();
		return $data;	
		
	}
	
	public function getHasilSuara()
	{
	  $data = $this->db->select('*')
				 ->from('paslon')
				 ->get()
				 ->result();
				
	  $counter = 0;
	  foreach($data as $paslon)
	  {
	  	$calon = $this->db->select('*')
						  ->from('pivot_paslon_calon')
					  	  ->join('calon', 'calon.id = pivot_paslon_calon.id_calon')
					      ->where('id_paslon', $paslon->id)
					  	  ->get()
					  	  ->result();
		
	  	$data[$counter]->calon = $calon;  
		$counter++;
	  } 
	
	  return $data;
					 
	}

	public function getSuaraPaslon($id){
		$query = $this->db->select('jumlah_suara')
						  ->from('paslon')
						  ->where('id',$id)
						  ->get()
						  ->row();

		return $query;
	}

}