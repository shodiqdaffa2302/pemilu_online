<?php

class Artikel_model extends Core_Model
{
    public function __construct()
    {
        self::$table            = 'artikel';
        self::$primaryKey       = 'id_artikel';
    }
    
    public function searchWithPagination($search,$key, $number, $offset){
        $this->db->order_by(self::$primaryKey,'DESC');
        if ($key) {
            $this->db->like($search,$key);
        }
        $query       = $this->db->get(self::$table, $number, $offset);
        if ($query->num_rows() > 0) {
            return $query->result();
        }

        return false;
    }
}