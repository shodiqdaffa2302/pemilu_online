<?php

class Admin_model extends Core_Model
{
    public function __construct()
    {
        self::$table            = 'admin';
        self::$primaryKey       = 'username';
    }

    public function checkLogin($data = array())
    {
        $check = $this->db->where('username', $data['username'])
                          ->get( self::$table )
                          ->row_array();

        if(!count($check))
        {
            return FALSE;
        }

        $checkPass = (password_verify($data['password'], $check['password']) );

        if($checkPass)
        {
            return $check;
        }
        else{
            return FALSE;
        }

    }

}