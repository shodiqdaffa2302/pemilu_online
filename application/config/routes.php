<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$route['default_controller'] = 'beranda';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;


#admin routes

$route['admin']                = "admin/dashboard/index"; 

$route['admin/artikel/delete']                = "admin/dashboard/index"; 


$route['admin/login']                         = "admin/login/index";
$route['admin/auth']                          = "admin/login/doLogin";
$route['admin/logout']                        = "admin/login/doLogout";


$route['admin/calon/delete-all']			  = "admin/calon/destroyAll";

$route['login']                               = "login/showLogin";
$route['auth']                                = "login/doLogin";
$route['logout']                              = "login/doLogout";

