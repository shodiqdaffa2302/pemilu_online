 <?php 

class Forum extends CI_Controller
{
	
	public function __construct(){
		parent::__construct();
		$this->load->model('Forum_model','fm');
	}

	public function index(){
		$config['base_url'] = base_url('forum/index');
		$config['total_rows'] = $this->fm->count_rows();  
		$config['per_page'] = 15;
		$config['full_tag_open']    = '<ul class="pagination">';
        $config['full_tag_close']   = '</ul>';
        $config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['first_tag_open']   = '<li>';
        $config['first_tag_close']  = '</li>';
        $config['prev_link']        = '&laquo';
        $config['prev_tag_open']    = '<li class="prev">';
        $config['prev_tag_close']   = '</li>';
        $config['next_link']        = '&raquo';
        $config['next_tag_open']    = '<li>';
        $config['next_tag_close']   = '</li>';
        $config['last_tag_open']    = '<li>';
        $config['last_tag_close']   = '</li>';
        $config['cur_tag_open']     = '<li class="active"><a href="">';
        $config['cur_tag_close']    = '</a></li>';
        $config['num_tag_open']     = '<li>';
        $config['num_tag_close']    = '</li>';
        $from = $this->uri->segment(3);
		$data['from'] = $from;
		$this->pagination->initialize($config);
		if (isset($_GET['forum'])) {
			$data['forum'] = $this->fm->searchWithComment('nama_thread',$_GET['forum'],$config['per_page'],$from);
		}else{
			$data['forum'] = $this->fm->allWithComment($config['per_page'],$from);
		}
		$this->load->view('forum',$data);
	}

	public function lihat($id){
		$data['terbaru']         = $this->fm->thread_newest();
		$data['komen']['jumlah'] = $this->fm->countComment($id);
		$data['komen']['isi']    = $this->fm->showComment($id);
		$data['forum']           = $this->fm->show($id);
		$this->load->view('lihat',$data);
	}

	public function komen($id){
		$this->form_validation->set_rules('nama','Nama','required');
		$this->form_validation->set_rules('isi','Isi','required');

		if (!$this->form_validation->run()) {
			$this->session->set_flashdata('err_msg',$this->form_validation->error_array());
			redirect($_SERVER['HTTP_REFERER']);
		}

		$nama = $this->input->post('nama');
		$isi  = $this->input->post('isi');

		$data = array(
				'nama'       => $nama,
				'isi'        => $isi,
				'waktu'      => date('Y-m-d H:i:s'),
				'id_thread'  => $id
			);

		$this->fm->create_comment($data);
        redirect($_SERVER['HTTP_REFERER']); 
	}

	// public function cari(){
	// 	$key = $this->input->get('cari');

		
	// }
}
