<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
require_once  APPPATH.'/core/Core_Pemilu.php';

class Pemilu extends Core_Pemilu
{
	
	function __construct()
	{
		parent::__construct();

		if(empty($this->session->userdata('credential2')))
		{
			redirect('login');
		}

		$this->load->model('Calon_model','cm');
	}

	public function index(){
		$data['template'] = "pemilu";
		$this->load->view('pilih',$data);
	}
	public function pilih(){
		$kunci = $this->session->userdata('credential2');
		$data['data']['calon'] = $this->cm->paslon();
		$pemilih = $this->cm->tampil_pemilih($kunci['nis']);
		if ($pemilih['status'] == 'y') {
			$data['template'] = "pilih2";
		}else{
			$data['template'] = 'peringatan';
		}
		$this->load->view('pilih',$data);
	}

	public function profil(){
		$data['data']['calon'] = $this->cm->paslon();
		$data['template'] = "profil";
		$this->load->view('pilih',$data);

	}

	function kirim(){
		$paslon = $this->input->post('pilih');
		$pemilih = $this->session->userdata('credential2');
		if (isset($paslon)) {
			$ambil = $this->cm->getSuaraPaslon($paslon);
			$hasil = $ambil->jumlah_suara + 1;

			$this->db->set(array('jumlah_suara' => $hasil))
					 ->where('id',$paslon)
					 ->update('paslon');
			$this->db->set(array('status' => 'n'))
					 ->where('nis',$pemilih['nis'])
					 ->update('pemilih');

			redirect(base_url('pemilu')); 
		}else{
			$this->session->set_flashdata('alert', 'Anda Belum Memilih Paslon');
			redirect('pemilu/pilih');
		}

	}

}


