<?php 

class Login extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('pemilih_model');
    }

    public function showLogin()
    {
        $this->load->view('pemilih/login');
    }

    public function doLogin()
    {
        $data = [
            'nis'  => $this->input->post('nis'),
            'password'  => $this->input->post('password')
        ];
        $this->form_validation->set_rules('nis','NIS','required');
        $this->form_validation->set_rules('password','Password','required');
        $run = $this->form_validation->run();
        
        if(!$run)
        {
            $this->session->set_flashdata('err_msg','Tidak Boleh Kosong');
            redirect($_SERVER['HTTP_REFERER']);
        }

        
        $checkLogin = $this->pemilih_model->checkLogin($data);
        
        if(FALSE == $checkLogin)
        {
            $this->session->set_flashdata('err_msg','Username atau password salah');
            redirect($_SERVER['HTTP_REFERER']);
        }

        $this->session->set_userdata('credential2', $checkLogin);
        redirect('pemilu');
    }

    public function doLogout()
    {
        $this->session->unset_userdata('credential2');
        
        redirect($_SERVER['HTTP_REFERER']);
    }

}