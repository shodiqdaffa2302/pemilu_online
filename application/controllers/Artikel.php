<?php 

class Artikel extends CI_Controller
{
	
	public function __construct(){
		parent::__construct();
		$this->load->model('Artikel_model','ma');
	}

	public function index(){
		$config['base_url'] = base_url('artikel/index');
		$config['total_rows'] = $this->ma->count_rows();  
		$config['per_page'] = 7;
		$config['full_tag_open']    = '<ul class="pagination">';
        $config['full_tag_close']   = '</ul>';
        $config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['first_tag_open']   = '<li>';
        $config['first_tag_close']  = '</li>';
        $config['prev_link']        = '&laquo';
        $config['prev_tag_open']    = '<li class="prev">';
        $config['prev_tag_close']   = '</li>';
        $config['next_link']        = '&raquo';
        $config['next_tag_open']    = '<li>';
        $config['next_tag_close']   = '</li>';
        $config['last_tag_open']    = '<li>';
        $config['last_tag_close']   = '</li>';
        $config['cur_tag_open']     = '<li class="active"><a href="">';
        $config['cur_tag_close']    = '</a></li>';
        $config['num_tag_open']     = '<li>';
        $config['num_tag_close']    = '</li>';
        $from = $this->uri->segment(3);
		$data['from'] = $from;
		$this->pagination->initialize($config);
		if (isset($_GET['artikel'])) {
			$data['artikel'] = $this->ma->searchWithPagination('nama_artikel',$_GET['artikel'],$config['per_page'],$from);
		}else{
			$data['artikel'] = $this->ma->pagging($config['per_page'],$from);
		}	
 		$this->load->view('artikel',$data);
	}
	public function baca($id){
		$data['artikel'] = $this->ma->show($id);
		$this->load->view('baca',$data);
	}

}


