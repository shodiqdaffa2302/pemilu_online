<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once  APPPATH.'/core/Core_Admin.php';

class Dashboard extends Core_Admin {

    /**
     * Halaman Admin Dahsboard
     */
    public function index()
    {
        $this->load->view('admin/dashboard');
    }
}
