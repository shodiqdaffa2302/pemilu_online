<?php 

require_once APPPATH.'/core/Core_Admin.php';
class Pemilih extends Core_Admin
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('pemilih_model','pm');
	}

	public function index(){
        $config['base_url'] = base_url('admin/pemilih/index');
        $config['total_rows'] = $this->pm->count_rows();  
        $config['per_page'] = 15;
        $config['full_tag_open']    = '<ul class="pagination">';
        $config['full_tag_close']   = '</ul>';
        $config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['first_tag_open']   = '<li>';
        $config['first_tag_close']  = '</li>';
        $config['prev_link']        = '&laquo';
        $config['prev_tag_open']    = '<li class="prev">';
        $config['prev_tag_close']   = '</li>';
        $config['next_link']        = '&raquo';
        $config['next_tag_open']    = '<li>';
        $config['next_tag_close']   = '</li>';
        $config['last_tag_open']    = '<li>';
        $config['last_tag_close']   = '</li>';
        $config['cur_tag_open']     = '<li class="active"><a href="">';
        $config['cur_tag_close']    = '</a></li>';
        $config['num_tag_open']     = '<li>';
        $config['num_tag_close']    = '</li>';
        $from = $this->uri->segment(4);
        $data['from'] = $from;
        $this->pagination->initialize($config);
        $data['pemilih'] = $this->pm->pagging($config['per_page'],$from);
		$this->load->view('admin/pemilih',$data);
	}

	public function add(){
		$this->load->view('admin/pemilih-new');
	}

	public function create(){
		$this->form_validation->set_rules('nis','NIS','required');
        $this->form_validation->set_rules('password','Password','required');
        $this->form_validation->set_rules('nama','Nama','required');
        $this->form_validation->set_rules('kelas','Kelas','required');

        if(!$this->form_validation->run())
        {
        	$this->session->set_flashdata('err_msg', $this->form_validation->error_array());
            redirect('admin/pemilih/add');
        }

        $data = [
            'nis' => $this->input->post('nis'),
            'password'  => password_hash($this->input->post('password'),PASSWORD_BCRYPT),
            'nama'        => $this->input->post('nama'),
            'kelas'     => $this->input->post('kelas')
        ];

        $this->pm->create($data);
        $this->session->set_flashdata('sc_msg','Berhasil Membuat Akun Baru');
        redirect('admin/pemilih'); 
	}
    public function edit($id)
    {
        $data['pemilih'] = $this->pm->show($id);

        if(!count($data))
        {
            $this->session->set_flashdata('err_msg','Data Tidak Ditemukan');
            redirect('admin/pemilih');
        }

        $this->load->view('admin/pemilih-edit', $data);
    }

    public function update($id)
    {
        $this->form_validation->set_rules('nama','Nama','required');
        $this->form_validation->set_rules('kelas','Kelas','required');

        if(!$this->form_validation->run())
        {
            $this->session->set_flashdata('err_msg',$this->form_validation->error_array());
            redirect($_SERVER['HTTP_REFERER']);
        }
    
        $data = [
            'password'  => password_hash($this->input->post('password'),PASSWORD_BCRYPT),
            'nama'        => $this->input->post('nama'),
            'kelas'     => $this->input->post('kelas')
        ];

        $this->pm->update($id,$data);
        $this->session->set_flashdata('sc_msg','Berhasil Mengedit Akun ');
        redirect('admin/pemilih'); 
    }

    public function delete($id)
    {
        $check                   = $this->pm->destroy($id);

        if(!$check)
        {
            $this->session->set_flashdata('err_msg','Data Tidak Ditemukan');
            redirect('admin/pemilih');
        }

        $this->session->set_flashdata('sc_msg','Berhasil Menghapus Akun');
        redirect('admin/pemilih');

    }
}


