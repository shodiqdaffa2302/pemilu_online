<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once  APPPATH.'/core/Core_Admin.php';

class Hasil extends Core_Admin {

    public function __construct()
    {
         parent::__construct();
         $this->load->model('calon_model');
    }


    public function index()
    {
        $data['data']['calon'] = $this->calon_model->getHasilSuara();
        $data['jumlah'] = $this->calon_model->jumlah_suara();
        $this->load->view('admin/hasil', $data);
    }

   
}
