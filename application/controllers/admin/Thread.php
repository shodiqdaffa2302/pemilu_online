<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once  APPPATH.'/core/Core_Admin.php';

class Thread extends Core_Admin {

     public function __construct()
     {
         parent::__construct();
         $this->load->model('Forum_model','fm');
     }

    public function index()
    {
        $config['base_url'] = base_url('admin/thread/index');
        $config['total_rows'] = $this->fm->count_rows();  
        $config['per_page'] = 10;
        $config['full_tag_open']    = '<ul class="pagination">';
        $config['full_tag_close']   = '</ul>';
        $config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['first_tag_open']   = '<li>';
        $config['first_tag_close']  = '</li>';
        $config['prev_link']        = '&laquo';
        $config['prev_tag_open']    = '<li class="prev">';
        $config['prev_tag_close']   = '</li>';
        $config['next_link']        = '&raquo';
        $config['next_tag_open']    = '<li>';
        $config['next_tag_close']   = '</li>';
        $config['last_tag_open']    = '<li>';
        $config['last_tag_close']   = '</li>';
        $config['cur_tag_open']     = '<li class="active"><a href="">';
        $config['cur_tag_close']    = '</a></li>';
        $config['num_tag_open']     = '<li>';
        $config['num_tag_close']    = '</li>';
        $from = $this->uri->segment(4);
        $data['from'] = $from;
        $this->pagination->initialize($config);
        $data['data']['thread'] = $this->fm->allWithComment($config['per_page'],$from);
        $this->load->view('admin/thread', $data);
    }

    public function add()
    {
         $this->load->view('admin/thread-new');
    }
    
    public function create()
    {
        $this->form_validation->set_rules('nama_thread','Judul Thread','required');
        $this->form_validation->set_rules('isi_thread','Content','required');

        if(!$this->form_validation->run())
        {
        	$this->session->set_flashdata('err_msg',$this->form_validation->error_array());
            redirect('admin/thread/add');
            // die();
        }

        $get_admin = $this->session->userdata('credential');

        $data = [
            'nama_thread' => $this->input->post('nama_thread'),
            'isi_thread'  => htmlspecialchars($this->input->post('isi_thread')),
            'waktu'       => date('Y-m-d H:i:s'),
            'nama'        => $get_admin['nama'] 
        ];

        $this->fm->create($data);
        $this->session->set_flashdata('sc_msg','Berhasil Membuat Thread Baru');
        redirect('admin/thread'); 
    }

    public function edit($id)
    {
        $data['data']['thread'] = $this->fm->show($id);

        if(!count($data))
        {
            $this->session->set_flashdata('err_msg','Data Tidak Ditemukan');
            redirect('admin/thread');
        }

        $this->load->view('admin/thread-edit', $data);
    }

    public function update($id)
    {
        $this->form_validation->set_rules('nama_thread','Judul Thread','required');
        $this->form_validation->set_rules('isi_thread','Content','required');

        if(!$this->form_validation->run())
        {
        	$this->session->set_flashdata('err_msg',$this->form_validation->error_array());
            redirect($_SERVER['HTTP_REFERER']);
        }

        $data = [
            'nama_thread' => $this->input->post('nama_thread'),
            'isi_thread'  => htmlspecialchars($this->input->post('isi_thread')),
            'waktu'        => date('Y-m-d H:i:s'),
            'username'     => 'admin'
        ];

        $this->fm->update($id,$data);
        $this->session->set_flashdata('sc_msg','Berhasil Mengedit Thread ');
        redirect('admin/thread'); 
    }

    public function delete($id)
    {
        $check                   = $this->fm->destroy($id);

        if(!$check)
        {
            $this->session->set_flashdata('err_msg','Data Tidak Ditemukan');
            redirect('admin/thread');
        }

        $this->session->set_flashdata('sc_msg','Berhasil Menghapus Data');
        redirect('admin/thread');

    }
}
