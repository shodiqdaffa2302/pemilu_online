<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once  APPPATH.'/core/Core_Admin.php';

class Calon extends Core_Admin {

    
	
    public function __construct()
    {
         parent::__construct();
         $this->load->model('calon_model');
    }

    public function index()
    {
        $data['data']['calon'] = $this->calon_model->paslon();

        $this->load->view('admin/calon', $data);
    }

    public function add()
    {
         $this->load->view('admin/calon-new');
    }
    
    public function create()
    {
    	$data_paslon = [
    		'visi' => $this->input->post('visi'),
    		'misi' => $this->input->post('misi'),
    		'warna' => '#'.$this->input->post('warna'),
    		'jumlah_suara' => 0
    	];
		
		$paslon = $this->calon_model->createPaslon($data_paslon);
		
    	$data_calon = [];
			
		$upload_config = [
			'upload_path' 	=> './img/',
			'allowed_types' => 'png|jpeg|jpg|gif|bmp',
			'max_size' 		=> '1000',
			'encrypt_name'  => TRUE,
		];
			
	    $this->load->library('upload', $upload_config);	
		
		for($i = 1; $i <= 2; $i++)
		{
			$data_calon = [
				'nama_calon' => $this->input->post('nama_calon_'.$i)
			];
			
			if(!$this->upload->do_upload('foto_'.$i))	
			{
			    $get_error = $this->upload->display_errors();
				$this->session->set_flashdata('err_msg', $get_error);
				redirect($_SERVER['HTTP_REFERER']);
			}
			else{
				$data_calon['gambar'] = $this->upload->data()['file_name'];
				$data_calon['status'] = $i;
				
				$this->calon_model->createCalonPaslon($data_calon, $paslon->id);
			}	
		}
		
		
		$this->session->set_flashdata('sc_msg', 'Berhasil menambah paslon dan calon baru');
		redirect('/admin/calon');
		
	
    }
	
	public function destroyAll()
	{
		$this->calon_model->destroyAll();
		
		$this->session->set_flashdata('sc_msg','Berhasil Menghapus Semua Data');
		redirect('admin/calon');
	}

    public function edit($id)
    {
        $data['data']['artikel'] = $this->calon_model->getPaslonWithCalon($id);
		// print_r($data); die();
        if(!count($data))
        {
            $this->session->set_flashdata('err_msg','Data Tidak Ditemukan');
            redirect('admin/calon');
        }

        $this->load->view('admin/calon-edit', $data);
    }

    public function update($id)
    {
		// var_dump(empty($_FILES['foto_1']['name'])); die();
	    $data_paslon = [
    		'visi' => $this->input->post('visi'),
    		'misi' => $this->input->post('misi'),
    		'warna' => '#'.$this->input->post('warna'),
    		'jumlah_suara' => 0
    	];
		
		$this->calon_model->updatePaslon($data_paslon, $id);
    	

		$data_calon = [];
			
		$upload_config = [
			'upload_path' 	=> UPLOAD_PATH,
			'allowed_types' => 'png|jpeg|jpg|gif|bmp',
			'max_size' 		=> '1000',
			'encrypt_name'  => TRUE,
		];
			
			$status = 1;
			$data_calon['nama_calon'] = $this->input->post('nama_calon_1');

			if (!empty($_FILES['foto_1']['name']))
			{
				$this->load->library('upload', $upload_config);	
			
					if(!$this->upload->do_upload('foto_1'))	
					{
						$get_error = $this->upload->display_errors();
						$this->session->set_flashdata('err_msg', $get_error);
						redirect($_SERVER['HTTP_REFERER']);
					}
					else{
						$data_calon['gambar'] = $this->upload->data()['file_name'];
						$data_calon['status'] = 1;
						
					}	
			}

			$this->calon_model->updateCalonPaslon($data_calon, $id, $status);

		unset($data_calon);
		unset($status);

		$data_calon['nama_calon'] = $this->input->post('nama_calon_2');
		$status = 2;

		if(!empty($_FILES['foto_2']['name']))
		{
			$this->load->library('upload', $upload_config);	


				
				if(!$this->upload->do_upload('foto_2'))	
				{
					$get_error = $this->upload->display_errors();
					$this->session->set_flashdata('err_msg', $get_error);
					redirect($_SERVER['HTTP_REFERER']);
				}
				else{
					$data_calon['gambar'] = $this->upload->data()['file_name'];
					$data_calon['status'] = 2;
					
				}	
		}

		$this->calon_model->updateCalonPaslon($data_calon, $id, $status);
		

		$this->session->set_flashdata('sc_msg', 'Berhasil mengedit paslon dan calon');
		redirect('/admin/calon');
	}

    public function delete($id)
    {
        $check                   = $this->calon_model->destroyPaslon($id);

        if(!$check)
        {
            $this->session->set_flashdata('err_msg','Data Tidak Ditemukan');
            redirect('admin/calon');
        }

        $this->session->set_flashdata('sc_msg','Berhasil Menghapus Data');
        redirect('admin/calon');

    }
}
