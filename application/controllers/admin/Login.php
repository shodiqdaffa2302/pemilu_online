<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->load->view('admin/login');
    }

    public function doLogin()
    {
        $data = [
            'username'  => $this->input->post('username'),
            'password'  => $this->input->post('password')
        ];

        $this->form_validation->set_rules('username','Username','required');
        $this->form_validation->set_rules('password','Password','required');
        $run = $this->form_validation->run();
        
        if(!$run)
        {
            $this->session->set_flashdata('err_msg','Tidak Boleh Kosong');
            redirect($_SERVER['HTTP_REFERER']);
        }

        $this->load->model('admin_model');
        
        $checkLogin = $this->admin_model->checkLogin($data);
        
        if(FALSE == $checkLogin)
        {
            $this->session->set_flashdata('err_msg','Username atau password salah');
            redirect($_SERVER['HTTP_REFERER']);
        }

        $this->session->set_userdata('credential', $checkLogin);
        redirect('admin/dashboard');
    }

    public function doLogout()
    {
        $this->session->unset_userdata('credential');
        
        redirect('/admin/login');
    }
}