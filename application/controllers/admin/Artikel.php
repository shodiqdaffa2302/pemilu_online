<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once  APPPATH.'/core/Core_Admin.php';

class Artikel extends Core_Admin {

     public function __construct()
     {
         parent::__construct();
         $this->load->model('artikel_model');
     }


    public function index()
    {
        $config['base_url'] = base_url('admin/artikel/index');
        $config['total_rows'] = $this->artikel_model->count_rows();  
        $config['per_page'] = 5;
        $config['full_tag_open']    = '<ul class="pagination">';
        $config['full_tag_close']   = '</ul>';
        $config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['first_tag_open']   = '<li>';
        $config['first_tag_close']  = '</li>';
        $config['prev_link']        = '&laquo';
        $config['prev_tag_open']    = '<li class="prev">';
        $config['prev_tag_close']   = '</li>';
        $config['next_link']        = '&raquo';
        $config['next_tag_open']    = '<li>';
        $config['next_tag_close']   = '</li>';
        $config['last_tag_open']    = '<li>';
        $config['last_tag_close']   = '</li>';
        $config['cur_tag_open']     = '<li class="active"><a href="">';
        $config['cur_tag_close']    = '</a></li>';
        $config['num_tag_open']     = '<li>';
        $config['num_tag_close']    = '</li>';
        $from = $this->uri->segment(4);
        $data['from'] = $from;
        $this->pagination->initialize($config);
        $data['data']['artikel'] = $this->artikel_model->pagging($config['per_page'],$from);
        $this->load->view('admin/artikel', $data);
    }

    public function add()
    {
         $this->load->view('admin/artikel-new');
    }
    
    public function create()
    {
        $this->form_validation->set_rules('nama_artikel','Judul Artikel','required');
        $this->form_validation->set_rules('isi_artikel','Content','required');

        if(!$this->form_validation->run())
        {
        	$this->session->set_flashdata('err_msg', $this->form_validation->error_array());
            redirect('admin/artikel/add');
        }

		#get Admin Data;
		$admin = $this->session->userdata('credential');

        $data = [
            'nama_artikel' => $this->input->post('nama_artikel'),
            'isi_artikel'  => htmlspecialchars($this->input->post('isi_artikel')),
            'waktu'        => date('Y-m-d H:i:s'),
            'nama'         => $admin['nama']
            // 'foto'         => 
        ];

        $this->artikel_model->create($data);
        $this->session->set_flashdata('sc_msg','Berhasil Membuat Artikel Baru');
        redirect('admin/artikel'); 
    }

    public function edit($id)
    {
        $data['data']['artikel'] = $this->artikel_model->show($id);

        if(!count($data))
        {
            $this->session->set_flashdata('err_msg','Data Tidak Ditemukan');
            redirect('admin/artikel');
        }

        $this->load->view('admin/artikel-edit', $data);
    }

    public function update($id)
    {
        $this->form_validation->set_rules('nama_artikel','Judul Artikel','required');
        $this->form_validation->set_rules('isi_artikel','Content','required');

        if(!$this->form_validation->run())
        {
        	$this->session->set_flashdata('err_msg',$this->form_validation->error_array());
            redirect($_SERVER['HTTP_REFERER']);
        }
	
		#get Admin Data;
		$_admin = $this->session->userdata('credential');

        $data = [
            'nama_artikel' => $this->input->post('nama_artikel'),
            'isi_artikel'  => htmlspecialchars($this->input->post('isi_artikel')),
            'waktu'        => date('Y-m-d H:i:s'),
            'username'     => $_admin->username
        ];

        $this->artikel_model->update($id,$data);
        $this->session->set_flashdata('sc_msg','Berhasil Mengedit Artikel ');
        redirect('admin/artikel'); 
    }

    public function delete($id)
    {
        $check                   = $this->artikel_model->destroy($id);

        if(!$check)
        {
            $this->session->set_flashdata('err_msg','Data Tidak Ditemukan');
            redirect('admin/artikel');
        }

        $this->session->set_flashdata('sc_msg','Berhasil Menghapus Data');
        redirect('admin/artikel');

    }
}
