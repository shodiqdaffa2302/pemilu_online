<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login Pemilih</title>
    <link rel="stylesheet" href="<?php echo base_url('assets/admin/css/animate.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/admin/css/bootstrap.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/admin/css/style.css') ?>">
    <script src="<?php echo base_url('assets/admin/js/jquery-3.1.1.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/admin/js/bootstrap.min.js') ?>"></script>
</head>
<body>
    <div class="container animated bounceInUp">
        <div class="container">
        <div class="wrap row animated bounceInUp">
            <div class="info-wrap col-md-7 clearfix">
                <h1>Selamat Datang di Halaman Login Pemilih</h1>
                <hr>
                <h4>Anda diwajibkan masuk terlebih dahulu untuk memilih paslon, gunakan kesempatan memilih anda sebaik - baik mungkin. Untuk password, silahkan menghubungi admin atau panitia pemilu</h4>
            </div>
            <div class="login-wrap col-md-4 col-md-offset-1">
               <div class="login-body">
                <h2 class="text-center">Login Pemilih</h2>
                <hr>
                <?php echo validation_errors(); ?>
                <form method="POST" action="<?=base_url('auth');?>">
                    <div class="form-group">
                        <input name="nis" type="text" class="form-control" placeholder="NIS">
                    </div>
                    <div class="form-group">
                        <input name="password" type="password" class="form-control" placeholder="Password">
                    </div>
                    <div class="form-group clearfix">
                        <button class="btn btn-custom pull-right btn-block">Login</button>
                    </div>
                    <div class="form-group clearfix">
                        <a href="<?=base_url()?>" class="btn btn-danger pull-right btn-block">Kembali</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>