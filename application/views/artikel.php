<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
	<title>Halaman Artikel</title>
	<link rel="stylesheet" href="<?php echo base_url('assets/font-awesome/css/font-awesome.css') ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/style.css') ?>">
</head>
<body class="artikel">
	<nav class="navbar navbar-fixed-top">
        <div class="container">
        	<div class="navbar-header">
        		<button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#nav">
	        		<span class="icon-bar"></span>
	        		<span class="icon-bar"></span>
	        		<span class="icon-bar"></span>
        		</button>
        	</div>
        	<div class="collapse navbar-collapse" id="nav">
	            <ul class="nav navbar-nav">
	            	<li><a href="<?php echo base_url('beranda') ?>">Beranda</a></li>
	                <li><a href="<?php echo base_url('artikel') ?>">Artikel</a></li>
	                <li><a href="<?php echo base_url('pemilu') ?>">Pemilu Online</a></li>
	                <li><a href="<?php echo base_url('forum') ?>">Forum</a></li>
	                <li><a href="<?php echo base_url('beranda/#tentang_kami') ?>">Tentang Kami</a></li>
	            </ul>
	            <form class="navbar-form navbar-right" method="GET">
					<div class="form-group">
						<input type="text" name="artikel" id="input" class="control" placeholder="Cari artikel disini">
					</div>
					<div class="btn btn-default"><span class="glyphicon glyphicon-search"></span></div>
				</form>
			</div>
        </div>
    </nav>

    <div class="artikel container">
    	<div class="page-satu">
	    	<div class="row">
	    	<?php if ($artikel): ?>
	    		<?php 
	    		$nomor = 1;
	    		foreach ($artikel as $key => $value){ 
	    			if ($nomor == 1) { ?>
			    		<div class="col-md-6">
				    		<div class="panel panel-default">
				    			<div class="panel-body imageWrapper">	
					    			<img src="<?php echo base_url('assets/img/artikel-post.jpg'); ?>" alt="" class="populer">
					    			<div class="cornerLink">
								    	<h4><?php echo $value->nama_artikel ?></h4>
										<p></p>
									    <a href="<?php echo base_url('artikel/baca/'.$value->id_artikel.'/'.url_title($value->nama_artikel,'-',TRUE)) ?>"><button class="btn btn-primary btn-md">Lihat</button></a>
								    </div>
					    			<div class="waktu">
					    				<h6><?php echo waktu_lalu($value->waktu) ?><i class="fa fa-clock-o"></i><h6 class="border"></h6></h6>
					    			</div>
				    			</div>
				    		</div>
				    	</div>
	    			<?php $nomor++; 

	    			}else{ ?>
	    			<div class="col-md-3">
			    		<div class="panel panel-default">
			    			<div class="panel-body imageWrapper">
			    				<img src="<?php echo base_url('assets/img/artikel-post2.jpeg'); ?>" alt="">
			    				<div class="cornerLink">
							    	<h4><?php echo $value->nama_artikel ?></h4>
							    	<p></p>
								    <a href="<?php echo base_url('artikel/baca/'.$value->id_artikel.'/'.url_title($value->nama_artikel,'-',TRUE)) ?>"><button class="btn btn-primary btn-md">Lihat</button></a>
							    </div>
				    			<div class="waktu">
				    				<h6><?php echo waktu_lalu($value->waktu) ?><i class="fa fa-clock-o"></i><h6 class="border"></h6></h6>
				    			</div>
			    			</div>
			    		</div>
			    	</div>
	    		<?php $nomor++;} } ?>
	    	<?php else: ?>
				<h2 class="text-center">Belum ada artikel</h2>
				<br>
	    	<?php endif;?>
		    </div>
    	</div>
    	<div class="page">
			<?php echo $this->pagination->create_links(); ?>
    	</div>
	</div>

	<div class="footer">
		<p>&copy; Struggle Team All rights Reserved</p>
	</div>

	<script type="text/javascript" src="<?php echo base_url('assets/jquery.min.js') ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js') ?>"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('.glyphicon-search').click(function(){
				$('#input').slideToggle();
			});

			$('#one').click(function(){
				$('.page-satu').css({'display':'initial'});
				$('.page-dua').hide();
			});

			$('#two').click(function(){
				$(this).css({'background-color':'blue','color':'#fff'});
				$('.pagination > a').css({'background-color':'white','color':'#505050'})
				$('.page-satu').hide();
				$('.page-dua').css({'display':'initial'});
			});
			
		});
	</script>
</body>
</html>
