<div class="panel panel-default">
<div class="panel-body">
		<h1>Pemilu Online</h1>
		</div>
</div>
<div class="panel panel-default">
	<div class="panel-body">
		<p>Selamat datang dihalaman pemilihan umum online. Berikut adalah tahapan-tahapan untuk memilih pasangan calon. </p><p>Sebelum memilih, diharapkan anda sudah mengetahui visi dan misi calon pasangan tersebut. Silahkan klik tombol <a href="<?=base_url('pemilu/profil')?>" class="btn btn-success btn-xs">Profil</a> untuk melihat visi misi.</p> <p>Jika anda sudah mengetahui visi dan misi calon pasangan, silahkan anda klik tombol <a href="<?=base_url('pemilu/pilih')?>" class="btn btn-primary btn-xs">Pilih</a> untuk memilih calon pasangan. </p><p>Ingat! anda hanya bisa memilih sekali, untuk itu, manfaatkan lah hak suara anda.</p>
	</div>
</div>
