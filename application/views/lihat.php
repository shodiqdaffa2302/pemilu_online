<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
	<title>Lihat</title>
	<link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/style.css') ?>">
	<link rel='stylesheet prefetch' href='https://fonts.googleapis.com/css?family=Alfa+Slab+One'>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/font-awesome/css/font-awesome.css') ?>">
</head>
<body class="lihat forum">
	<div class="header">
		<div class="container">
			<h1>Struggle LCW</h1>
			<div class="navbar-header">
        		<button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#nav">
	        		<span class="icon-bar"></span>
	        		<span class="icon-bar"></span>
	        		<span class="icon-bar"></span>
        		</button>
        	</div>
		</div>
	</div>
	<nav class="navbar navbar-default">
        <div class="container">
        	<div class="collapse navbar-collapse" id="nav">
	            <ul class="nav navbar-nav">
	            	<li><a href="<?php echo base_url('beranda') ?>">Home</a></li>
	                <li><a href="<?php echo base_url('artikel') ?>">Artikel</a></li>
	                <li><a href="<?php echo base_url('pemilu') ?>">Pemilu Online</a></li>
	                <li><a href="<?php echo base_url('forum') ?>">Forum</a></li>
	                <li><a href="<?php echo base_url('beranda/#tentang_kami') ?>">Tentang Kami</a></li>
	            </ul>
	            <ul class="nav navbar-nav navbar-right">
	            	<?php if ($this->session->userdata('credential2')): ?>
						<li><a href="<?=base_url('logout')?>">Logout</a></li>
	            	<?php else: ?>
		            	<li><a href="<?= base_url('login'); ?>">Login</a></li>		            		
	            	<?php endif ?>
	            </ul>
	        </div>
        </div>
    </nav>
    <div class="container">
    	<div class="post col-md-8">
    		<div class="thread">
				<h2><?=$forum->nama_thread?></h2>
				<div class="desc">
					<?php 
						$isi = preg_replace("/<[^>]*>/"," ", htmlspecialchars_decode($forum->isi_thread));
						echo $isi; 
 					?>
				</div>
				<p class="status">
					<a href="#" class="fa fa-comments-o">  <?=$komen['jumlah']?></a>
					<a class="pull-right fa fa-calendar"> <?=waktu_lalu($forum->waktu)?></a>
				</p>
			</div>
			<hr>
			
			<div class="komentar">
			<?php if ($komen['isi']): ?>
				<?php foreach ($komen['isi'] as $komentar): ?>
					<div class="col-md-1">
						<img src="<?php echo base_url('assets/img/icon-pemilu.png') ?>" alt="" class="img img-circle">
					</div>
					<div class="col-md-11">
						<h6><?=$komentar->nama?></h6>
						<p><?=$komentar->isi?></p>
						<ul class="status">
							<li class="waktu"><?=waktu_lalu($komentar->waktu)?></li>
						</ul>
					</div>
				<?php endforeach ?>
			<?php else: ?>
				<div class="col-md-12">
					<h3 class="text-center">Tidak Ada Komentar</h3>
				</div>
			<?php endif ?>
			</div>

			<hr>
			<div class="ngomen col-md-12">
			<?php if(!empty($this->session->flashdata('err_msg'))): ?>
				<div class="alert alert-danger">
			    <?php if(is_array($this->session->flashdata('err_msg'))) : ?>
			            <?php foreach($this->session->flashdata('err_msg') as $error) : ?>
			                    <?=$error;?>    
			            <?php endforeach; ?>
			    <?php else: ?>
			        
			            <?=$this->session->flashdata('err_msg');?>
			        
			    <?php endif; ?>
			    </div>
			<?php endif; ?>
				<form action="<?=base_url('forum/komen/'.$forum->id_thread);?>" method="POST">
				<?php if ($this->session->userdata('credential2')): ?>
					<?php 
						$user = $this->session->userdata('credential2');
					 ?>
					<input type="text" name="nama" placeholder="Nama" class="form-control" value="<?=$user['nama']?>" readonly>
				<?php else: ?>
					<input type="text" name="nama" placeholder="Nama" class="form-control">
				<?php endif ?>
					<textarea name="isi" id="" cols="30" rows="5" placeholder="Balas Thread .." class="form-control"></textarea>
					<button type="submit" class="btn btn-md">Kirim</button>
				</form>
			</div>

    	</div>
    	<div class="popular-post col-md-4">
    		<div class="panel">
    			<div class="panel-heading">
    				<h4 class="latest-head">Thread Terbaru</h4>
    				<hr>
    			</div>
    			<?php foreach ($terbaru as $key => $value): ?>
    				<a href="<?=base_url('forum/lihat/'.$value->id_thread)?>">
			    		<div class="panel-body">
				    		<p class="judul-thread"><?=$value->nama_thread?></p>
				    		<p class="post-popular-post">
				    			<?php 
									$isi = preg_replace("/<[^>]*>/"," ", htmlspecialchars_decode($value->isi_thread));
									$hasil = (strlen($isi) <= 200) ? $isi : substr($isi,0,200).'...';
									echo $hasil; 
 								?>
 							</p>
			    		</div>
		    		</a>
    			<?php endforeach ?>
    		</div>
    	</div>
    </div>

    <div class="footer">
		Struggle LCW &copy; 2017
	</div>
	
	<script src="<?php echo base_url('assets/jquery.min.js') ?>"></script>
	<script src="<?php echo base_url('assets/bootstrap/js/bootstrap.js') ?>"></script>
</body>
</html>
