<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
	<title>Baca Artikel</title>
	<link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.css') ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/style.css') ?>">
</head>
<body class="baca-artikel">
	<nav class="navbar navbar-fixed-top nav-baca">
        <div class="container">
        	<div class="navbar-header">
        		<button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#nav">
	        		<span class="icon-bar"></span>
	        		<span class="icon-bar"></span>
	        		<span class="icon-bar"></span>
        		</button>
        	</div>
        	<div class="collapse navbar-collapse" id="nav">
	            <ul class="nav navbar-nav">
	            	<li><a href="<?php echo base_url('beranda') ?>">Home</a></li>
	                <li><a href="<?php echo base_url('artikel') ?>">Artikel</a></li>
	                <li><a href="<?php echo base_url('pemilu') ?>">Pemilu Online</a></li>
	                <li><a href="<?php echo base_url('forum') ?>">Forum</a></li>
	                <li><a href="<?php echo base_url('beranda/#tentang_kami') ?>">Tentang Kami</a></li>
	            </ul>
           <!--  <ul class="nav navbar-nav navbar-right">
            	<li><a href=""><span class="glyphicon glyphicon-log-in"></span>Login</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span>Yeppy Mangun P <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Akun saya</a></li>
                        <li><a href="#">Pengaturan</a></li>
                        <li><a href="#">Keluar</a></li>
                    </ul>
                </li>
            </ul> -->
	            <!-- <form class="navbar-form navbar-right">
					<div class="form-group">
						<input type="text" class="form-control" placeholder="Cari">
					</div>
					<button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
				</form> -->
	        </div>
        </div>
    </nav>
	<img src="<?php echo base_url('assets/img/banner.jpg') ?>" alt="">
	<div class="struktur-artikel">
		<div class="container">
			<div class="col-md-2">
				<p class="judul-artikel"><?php echo $artikel->nama_artikel ?></p>
			</div>
			<div class="col-md-8">
				<?php 			    				
					$isi = preg_replace("/<[^>]*>/"," ", htmlspecialchars_decode($artikel->isi_artikel));
					echo $isi; ?>
			</div>
			<div class="col-md-2">
				<p class="sub-tittle">
					<?php echo waktu_lalu($artikel->waktu) ?> <br>
					Di posting oleh <br>
					<a class="postman" href="#"><?php echo $artikel->nama ?></a>
				</p>
			</div>
		</div>
	</div>
	<div class="footer">
		<p>&copy; 2017 Struggle Team</p>
	</div>

	<script type="text/javascript" src="<?php echo base_url('assets/jquery.min.js') ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js') ?>"></script>
	<script type="text/javascript">
		$(document).ready(function (){
			$(window).scroll(function() {
				if ($(this).scrollTop()>300){
					$('.navbar-fixed-top').css({'background-color':'rgba(44, 62, 80, 0.85)','transition':'1.5s'});
				}
				else{
					$('.navbar-fixed-top').css({'background-color':'transparent'});
				}
			});
		});
	</script>
</body>
</html>
