<?php if ($data['calon']): ?>
<?php $counter = 1; $x=1;?>
<?php foreach($data['calon'] as $calon) : ?>
  <div class="calon-satu col-md-4">
    <h3><b>Paslon <?=$counter;?></b> </h3>
    <?php foreach($calon->calon as $cal) : ?>
	  		<?=$cal->nama_calon;?>
	  		<?php if ($x==1): ?>
	  			<?php echo " & "; ?>
	  		<?php endif ?>
	  		<?php $x++; ?>
		<?php endforeach; ?>
		<?php $x=1; ?><br>
	  	<?php foreach($calon->calon as $cal) : ?>
	  		<img src="<?=base_url('img/'.$cal->gambar);?>" />
		<?php endforeach; ?>	  
    	<button class="block btn btn-primary" data-toggle="modal" data-target="#visimisi">Lihat Visi & Misi</button>
    	<div class="modal fade" id="visimisi">
    		<div class="modal-dialog">
		    	<div class="modal-content">
		    		<div class="modal-header">
		    			<button class="close" data-dismiss="modal">&times;</button>
		    			<h4>Visi & Misi</h4>
		    		</div>
		    		<div class="modal-body">
		    			<b>Visi</b> <br>
		    			<p><?=$calon->visi;?></p><br>
		    			<b>Misi</b> <br>
		    			<p><?=$calon->misi;?></p>
		    		</div>
		    	</div>
	    	</div>
    	</div>
	  <span>
	  <span></span>
	</div>	  	
  <?php $counter++; ?>
<?php endforeach; ?>	
<?php else: ?>
	<h2 class="text-center">Belum ada paslon</h2>
<?php endif ?>
