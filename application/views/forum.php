<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
	<title>Forum</title>
	<link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/style.css') ?>">
	<link rel='stylesheet prefetch' href='https://fonts.googleapis.com/css?family=Alfa+Slab+One'>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/font-awesome/css/font-awesome.css') ?>">
	
</head>
<body class="forum">
	<div class="header">
		<div class="container">
			<h1>Struggle LCW</h1>
			<div class="navbar-header">
        		<button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#nav">
	        		<span class="icon-bar"></span>
	        		<span class="icon-bar"></span>
	        		<span class="icon-bar"></span>
        		</button>
        	</div>
			<form class="navbar-form" method="GET">
				<div class="form-group">
					<input type="text" class="form-control" name="forum" placeholder="Cari">
				</div>
				<button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
			</form>
		</div>
	</div>
	<nav class="navbar navbar-default">
        <div class="container">
        	<div class="collapse navbar-collapse" id="nav">
	            <ul class="nav navbar-nav">
	            	<li><a href="<?php echo base_url('beranda') ?>">Beranda</a></li>
	                <li><a href="<?php echo base_url('artikel') ?>">Artikel</a></li>
	                <li><a href="<?php echo base_url('pemilu') ?>">Pemilu Online</a></li>
	                <li><a href="<?php echo base_url('forum') ?>">Forum</a></li>
	                <li><a href="<?php echo base_url('beranda/#tentang_kami') ?>">Tentang Kami</a></li>
	            </ul>
	            <ul class="nav navbar-nav navbar-right">
	            	<?php if ($this->session->userdata('credential2')): ?>
						<li><a href="<?=base_url('logout')?>">Logout</a></li>
	            	<?php else: ?>
		            	<li><a href="<?= base_url('login'); ?>">Login</a></li>		            		
	            	<?php endif ?>
	            </ul>
	            <form class="navbar-form" method="GET">
					<div class="form-group">
						<input type="text" class="form-control" name="forum" placeholder="Cari">
					</div>
					<button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
				</form>
	        </div>
        </div>
    </nav>
    <div class="container">
    	<div class="post col-md-12">
    		<div class="panel panel-default">
    			<div class="panel-heading">
    				<h4 class="latest-head">Thread</h4>
    				<hr>
    			</div>
    			
    			<?php foreach ($forum as $key => $value): ?>
    				<div class="panel-body">
		    			<div class="col-md-1">
		    				<p><img src="<?php echo base_url('assets/img/business.png') ?>" alt=""></p>
		    			</div>
		    			<a href="<?php echo base_url('forum/lihat/'.$value->id_thread); ?>">
			    			<div class="col-md-11">
			    				<h4><b><?=$value->nama_thread?></b></h4>
			    				<h6 class="keterangan"><?=waktu_lalu($value->waktu)?> <span class="fa fa-calendar"></span></h6>
			    				<div class="desc">
			    				<?php 
			    				$isi = preg_replace("/<[^>]*>/"," ", htmlspecialchars_decode($value->isi_thread));
								$hasil = (strlen($isi) <= 200) ? $isi : substr($isi, 0, 200) . ' ...';
								echo $hasil; 
								?>
			    				</div>
			    				<p class="status">
				    				<a href="#" class="fa fa-comments-o"> <?=$value->komentar?></a>
			    				</p>
			    			</div>
		    			</a>
		    		</div>
    			<?php endforeach ?>
    		</div>
    		<?php echo $this->pagination->create_links(); ?>
    	</div>
    </div>
    <div class="footer">
		Struggle LCW &copy; 2017
	</div>

	<script src="<?php echo base_url('assets/jquery.min.js') ?>"></script>
	<script src="<?php echo base_url('assets/bootstrap/js/bootstrap.js') ?>"></script>
</body>
</html>
