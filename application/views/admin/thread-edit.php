<?php $this->load->view('admin/partial/header'); ?>

<div class="row">
    <div class="col-md-12">
        <?php echo validation_errors(); ?>
        <form action="<?=base_url('admin/thread/update/' . $data['thread']->id_thread);?>" method="POST">
            <div class="form-group">
                <label>Judul Thread * </label>
                <?=form_error('nama_thread');?>
                <input type="text" name="nama_thread" class="form-control" value="<?=(!empty(set_value('nama_thread'))) ? set_value('nama_thread') : $data['thread']->nama_thread;?>" />
            </div>
             <div class="form-group">
                <label>Content * </label>
                <?=form_error('isi_thread');?>
                <textarea name="isi_thread" id="artikel_content" class="form-control" style="height:500px; background: white !important;">
                   <?=(!empty(set_value('isi_thread'))) ? set_value('isi_thread') : $data['thread']->isi_thread ;?>
                </textarea>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-success">Simpan</button>
            </div>

        </form>
    </div>
</div>

<?php $this->load->view('admin/partial/footer'); ?>