  <?php $this->load->view('admin/partial/header'); ?>

<div class="row">
  <div class="col-md-12">
  	<canvas id="chart">
  		
  	</canvas>
  	<script src="<?=base_url('assets/bower_components/chart.js/dist/Chart.min.js');?>" type="text/javascript" charset="utf-8"></script>
  	<script>
      $x = 1;
  		var labels  = [
  			<?php foreach($data['calon'] as $calon) : ?>
          <?php $x=1; ?>
  				"<?php foreach($calon->calon as $calonlon) : ?><?=$calonlon->nama_calon;?><?php if($x==1):?> - <?php endif?><?php $x++; ?><?php endforeach ; ?>"<?php $x=1; ?>,
  			<?php endforeach; ?>
  		];
  			
  		var suara = [
  			<?php foreach($data['calon'] as $calon) : ?>
  				"<?=$calon->jumlah_suara;?>",
  			<?php endforeach; ?>
  		];
  		
  		var bgColor = [
  			<?php foreach($data['calon'] as $calon) : ?>
  				"<?=$calon->warna;?>",
  			<?php endforeach; ?>
  		];
  		
  		var data = {
		    labels: labels,
		    datasets: [
		        {
		            label: "Perolehan Suara",
		            backgroundColor: bgColor,
		            borderWidth: 1,
		            data: suara,
		        }
		    ]
		};
		
		var ctx = document.getElementById('chart');
	
		var options = {
			display : true,
			responsive: true,
      scales: {
                  yAxes: [{
                          ticks: {
                              beginAtZero: true,
                              max : <?=$jumlah->jumlah_suara; ?>
                          }
                      }]
              }
		};

		var myBarChart = new Chart(ctx, {
		    type: 'bar',
		    data: data,
		    options: options
		});
  	</script>
    <p class="text-center"><b>Total Suara Masuk : </b><?=$jumlah->jumlah_suara?> Suara</p>
  </div>
</div>


<?php $this->load->view('admin/partial/footer'); ?>
