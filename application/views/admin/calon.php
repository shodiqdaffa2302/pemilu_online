<?php $this->load->view('admin/partial/header'); ?>

 	<a href="<?= base_url('admin/calon/add'); ?>" class="btn btn-success">Tambah Paslon dan Calon</a>
 	<a href="<?= base_url('admin/calon/delete-all'); ?>" class="btn btn-danger" onclick="return confirm('Yakin Hapus Semua ?')">Hapus Semua</a>
 	<br>
 	<br>
 	<br>
 	

	<?php $counter = 1; $x=1 ?>
	<?php foreach($data['calon'] as $calon) : ?>
	  <div class="calon-satu col-md-4">
	    <h3><b>Paslon <?=$counter;?></b> </h3>
		  	<?php foreach($calon->calon as $cal) : ?>
		  		<?=$cal->nama_calon;?>
		  		<?php if ($x==1): ?>
		  			<?php echo " & "; ?>
		  		<?php endif ?>
		  		<?php $x++; ?>
			<?php endforeach; ?>
			<?php  $x=1;?>
	    <h4>Calon <?=$counter;?> </h4>
	    <div id="name">
		  	<?php foreach($calon->calon as $cal) : ?>
		  		<img src="<?=base_url('img/'.$cal->gambar);?>" />
			<?php endforeach; ?>	  
		</div>
	    <a href="<?=base_url('admin/calon/edit/'.$calon->id);?>" class="btn btn-info">Edit</a>
	 	<a href="<?=base_url('admin/calon/delete/'.$calon->id);?>" class="btn btn-danger">Hapus</a>
		<button class="btn btn-primary" data-toggle="modal" data-target="#visimisi-<?=$counter;?>">Lihat Visi & Misi</button>
	    <div id="name">
	    	<div id="visimisi-<?=$counter;?>" class="modal fade">
	    		<div class="modal-dialog">
	    			<div class="modal-content">
	    				<div class="modal-header">
	    					<button class="close" data-dismiss="modal">&times;</button>
	    					<h4 class="modal-title">Visi & Misi Paslon <?=$counter?></h4>
	    				</div>
	    				<div class="modal-body">
	    					<b>Visi</b><br>
	    					<p><?php echo $calon->visi; ?></p><br>
	    					<b>Misi</b><br>
	    					<p><?php echo $calon->misi; ?></p>
	    				</div>
	    			</div>
	    		</div>
	    	</div>
		</div>
	  </div>
	  <?php $counter++; ?>
	<?php endforeach; ?>
	
<?php $this->load->view('admin/partial/footer'); ?>