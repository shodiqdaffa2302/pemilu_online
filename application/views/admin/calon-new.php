<?php $this->load->view('admin/partial/header'); ?>

	<div class="row">
		<div class="col-md-12">
		  <form action="<?=base_url('admin/calon/create');?>" method="POST" enctype="multipart/form-data" >
			<div class="form-group">
			  <label>Visi</label>
			  <textarea class="form-control" name="visi" rows="8" cols="40"></textarea>
			</div>
			
			<div class="form-group">
				<label>Misi</label>
				<textarea class="form-control" name="misi" rows="8" cols="40"></textarea>
			</div>
			
			<div class="form-group">
				<label>Warna</label>
				<script src="<?=base_url('assets/js/jscolor/jscolor.min.js');?>" type="text/javascript" charset="utf-8"></script>
				<input type="text" class="form-control jscolor" name="warna" rows="8" cols="40" value="e74c3c" />
			</div>		
			
			<div class="row">
			  <div class="col-md-6">
				<h4 class="text-center">Ketua</h4>
				<br />
				
				<div class="form-group">
					<label>Foto</label>
					<input class="form-control" type="file" name="foto_1"  />
				</div>
				
				<div class="form-group">
				  	<label>Nama</label>
				  	<input class="form-control" type="text" name="nama_calon_1"  accept="image/*" />
				</div>
			  </div>
			  
			  <div class="col-md-6">
				<h4 class="text-center">Wakil</h4>
				<br />
				
				<div class="form-group">
					<label>Foto</label>
					<input class="form-control" type="file" name="foto_2"  />
				</div>
				<div class="form-group">
				  	<label>Nama</label>
				  	<input class="form-control" type="text" name="nama_calon_2" value=""/>
				</div>
			  </div>
			</div>
			
			<br />
			<div class="form-group">
			  <button class="btn btn-success">Simpan</button>
			</div>
		  </form>
		</div>
	</div>

<?php $this->load->view('admin/partial/footer'); ?>