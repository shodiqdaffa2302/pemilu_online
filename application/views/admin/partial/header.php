<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>Admin Panel | Struggle Website</title>
    <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/style.css') ?>">
  </head>
  <body class="pilih">
    <nav class="navbar navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#nav">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <div class="navbar-brand"><a href="<?=base_url('beranda')?>">Pemilu Online</a></div>
        </div>
        <div class="collapse navbar-collapse" id="nav">
          <ul class="nav navbar-nav navbar-right">
                <li>
              <a class="" href="<?=base_url('admin/logout');?>">Logout</a>
            </li>
        </div>
      </div>
    </nav>
    <div class="container-side-bar">
        <div class="wrapper">
              <div class="side-bar">
                    <ul class="navbar">
                        <li class="menu-head">ADMIN PANEL<a href="#" class="push_menu"><span class="glyphicon glyphicon-align-justify pull-right"></span></a>
                        </li>
                        <div class="menu">
                            <li>
                                <a  href="<?php echo base_url('admin/dashboard') ?>">Dashboard</a>
                            </li>
                            <li>
                                <a  href="<?php echo base_url('admin/artikel') ?>">Artikel</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('admin/thread') ?>">Thread</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('admin/calon') ?>">Calon</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('admin/hasil') ?>">Hasil Suara</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('admin/pemilih') ?>">Akun Pemilih</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('admin/akun') ?>">Akun Admin</a>
                            </li>
                        </div>
                        
                    </ul>
              </div>   
              <div class="content">
              <?php $this->load->view('admin/partial/notification'); ?>