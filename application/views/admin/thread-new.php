<?php $this->load->view('admin/partial/header'); ?>

<div class="row">
    <div class="col-md-12">
        
        <form action="<?=base_url('admin/thread/create');?>" method="POST">
            <div class="form-group">
                <label>Judul Thread * </label>
                
                <input type="text" name="nama_thread" class="form-control" value="<?=set_value('nama_thread');?>" />
            </div>
             <div class="form-group">
                <label>Content * </label>
                
                <textarea name="isi_thread" id="artikel_content" class="form-control" style="height:500px; background: white !important;">
					<?=set_value('isi_thread');?>
                </textarea>

            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-success">Simpan</button>
            </div>

        </form>
    </div>
</div>

<?php $this->load->view('admin/partial/footer'); ?>