<?php $this->load->view('admin/partial/header'); ?>

<a href="<?=base_url('admin/thread/add');?>" class="btn btn-success">Buat Thread Baru</a>
<br>
<br>
<table class="table table-hover">
    <thead>
        <th>Judul</th>
        <th>Content</th>
        <th>Waktu</th>
        <th>Penulis</th>
        <th>Komentar</th>
        <th>Aksi</th>
    </thead>
    <tbody>
        <?php if ($data['thread']): ?>
        <?php foreach($data['thread']  as $thread) : ?>
            <tr>
                <td><?=$thread->nama_thread;?></td>
                <td><?php
                	$isi = preg_replace("/<[^>]*>/"," ", htmlspecialchars_decode($thread->isi_thread));
					$hasil = (strlen($isi) <= 200) ? $isi : substr($isi, 0, 200) . ' ...';
					echo $hasil;
				?></td>
                <td><?=waktu_lalu($thread->waktu);?></td>
                <td><?=$thread->nama;?></td>
                <td><?=$thread->komentar;?></td>
                <td>
                    <a href="<?=base_url('admin/thread/edit/' . $thread->id_thread);?>" class="btn btn-info btn-xs">Edit</a> 
                    <a href="<?=base_url('admin/thread/delete/' . $thread->id_thread);?>" class="btn btn-danger btn-xs">Hapus</a>
                </td>
            </tr>
        <?php endforeach; ?>
        <?php else: ?>
            <tr>
                <td colspan="6" class="text-center">Belum ada thread</td>
            </tr>            
        <?php endif ?>
    </tbody>
</table>

<?php $this->load->view('admin/partial/footer'); ?>