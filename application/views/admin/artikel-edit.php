<?php $this->load->view('admin/partial/header'); ?>

<div class="row">
    <div class="col-md-12">
        <?php echo validation_errors(); ?>
        <form action="<?=base_url('admin/artikel/update/' . $data['artikel']->id_artikel);?>" method="POST">
            
            <div class="form-group">
                <label>Judul Artikel * </label>
                <input type="text" name="nama_artikel" class="form-control" value="<?=(!empty(set_value('nama_artikel'))) ? set_value('nama_artikel') : $data['artikel']->nama_artikel;?>" />
            </div>

             <div class="form-group">
                <label>Content * </label>
                <textarea name="isi_artikel" id="artikel_content" class="form-control" style="height:500px; background: white !important;">
                   <?=(!empty(set_value('isi_artikel'))) ? set_value('isi_artikel') : $data['artikel']->isi_artikel ;?>
                </textarea>

            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-success">Simpan</button>
            </div>

        </form>
    </div>
</div>

<?php $this->load->view('admin/partial/footer'); ?>