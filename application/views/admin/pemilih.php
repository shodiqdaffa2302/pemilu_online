<?php $this->load->view('admin/partial/header'); ?>
<a href="<?=base_url('admin/pemilih/add');?>" class="btn btn-success">Tambah Pemilih</a>
<br>
<br>
<table class="table table-hover">
    <thead>
        <th>NIS</th>
        <th>Nama</th>
        <th>Kelas</th>
        <th>Status</th>
        <th>Aksi</th>
    </thead>
    <tbody>
    <?php foreach ($pemilih as $key => $value): ?>
        <tr>
            <td><?=$value->nis?></td>
            <td><?=$value->nama?></td>
            <td><?=$value->kelas?></td>
            <td>
                <?php if ($value->status == 'n'): ?>
                    <?php echo 'Sudah memilih' ?>
                <?php else: ?>
                    <?php echo 'Belum memilih' ?>
                <?php endif ?>
            </td>
            <td>
                <a href="<?=base_url('admin/pemilih/edit/'.$value->nis)?>" class="btn btn-info btn-xs">Edit</a> 
                <a href="<?=base_url('admin/pemilih/delete/'.$value->nis)?>" class="btn btn-danger btn-xs">Hapus</a>
            </td>
        </tr>
    <?php endforeach ?>
    </tbody>
</table>
<?php $this->load->view('admin/partial/footer'); ?>