<?php $this->load->view('admin/partial/header'); ?>

<a href="<?=base_url('admin/artikel/add');?>" class="btn btn-success">Buat Artikel Baru</a>
<br>
<br>
<table class="table table-hover">
    <thead>
        <th>Judul</th>
        <th>Content</th>
        <th>Waktu</th>
        <th>Penulis</th>
        <th>Aksi</th>
    </thead>
    <tbody>
    <?php if ($data['artikel']): ?>
        <?php foreach($data['artikel']  as $artikel) : ?>
            <tr>
                <td><?=$artikel->nama_artikel;?></td>
                <td><?php
                	$isi = preg_replace("/<[^>]*>/"," ", htmlspecialchars_decode($artikel->isi_artikel));
					$hasil = (strlen($isi) <= 200) ? $isi : substr($isi, 0, 200) . ' ...';
					echo $hasil;
				?></td>
                <td><?=waktu_lalu($artikel->waktu);?></td>
                <td><?=$artikel->nama;?></td>
                <td>
                    <a href="<?=base_url('admin/artikel/edit/' . $artikel->id_artikel);?>" class="btn btn-info btn-xs">Edit</a> 
                    <a href="<?=base_url('admin/artikel/delete/' . $artikel->id_artikel);?>" class="btn btn-danger btn-xs">Hapus</a>
                </td>
            </tr>
        <?php endforeach; ?>
    <?php else: ?>
        <tr>
            <td colspan="5" class="text-center">Belum ada artikel</td>
        </tr>    
    <?php endif ?>
    </tbody>
</table>
<?=$this->pagination->create_links(); ?>

<?php $this->load->view('admin/partial/footer'); ?>