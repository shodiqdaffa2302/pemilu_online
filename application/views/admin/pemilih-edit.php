<?php $this->load->view('admin/partial/header'); ?>

<div class="row">
    <div class="col-md-12">
        <?php echo validation_errors(); ?>
        <form action="<?=base_url('admin/pemilih/update/'.$pemilih->nis);?>" method="POST">
            <div class="form-group">
                <label>Password Baru *</label>
                <input type="password" name="password" class="form-control" value="<?php echo set_value('password'); ?>">
            </div>
            <div class="form-group">
                <label>Nama *</label>
                <input type="text" name="nama" class="form-control" value="<?=(!empty(set_value('nama'))) ? set_value('nama') : $pemilih->nama ;?>">
            </div>
            <div class="form-group">
                <label>Kelas *</label>
                <input type="text" name="kelas" class="form-control" value="<?=(!empty(set_value('kelas'))) ? set_value('kelas') : $pemilih->kelas ;?>">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-success">Update</button>
            </div>

        </form>
    </div>
</div>

<?php $this->load->view('admin/partial/footer'); ?>