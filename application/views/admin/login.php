<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login Admin Panel</title>
    <link rel="stylesheet" href="<?php echo base_url('assets/admin/css/animate.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/admin/css/bootstrap.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/admin/css/style.css') ?>">
    <script src="<?php echo base_url('assets/admin/js/jquery-3.1.1.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/admin/js/bootstrap.min.js') ?>"></script>
</head>
<body>
    <div class="container center animated bounceInUp">
        <div class="login-wrap ">
               <div class="login-body">
                <h2 class="text-center">Login Admin</h2>
                <hr>
                <?php echo validation_errors(); ?>
                <form method="POST" action="<?=base_url('admin/auth');?>">
                    <div class="form-group">
                        <input name="username" type="text" class="form-control" placeholder="Username">
                    </div>
                    <div class="form-group">
                        <input name="password" type="password" class="form-control" placeholder="Password">
                    </div>
                    <div class="form-group clearfix">
                        <button class="btn btn-custom pull-right btn-block">Login</button>
                    </div>
                    <div class="form-group clearfix">
                        <a href="<?=base_url()?>" class="btn btn-danger btn-block pull-right">Kembali</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>