<?php if(!empty($this->session->flashdata('err_msg'))): ?>
	<div class="alert alert-danger">
    <?php if(is_array($this->session->flashdata('err_msg'))) : ?>
        <ul>
            <?php foreach($this->session->flashdata('err_msg') as $error) : ?>
                <li>
                    <?=$error;?>    
                </li>    
            <?php endforeach; ?>
        </ul>
    <?php else: ?>
        
            <?=$this->session->flashdata('err_msg');?>
        
    <?php endif; ?>
    </div>
<?php endif; ?>
<?php if(!empty($this->session->flashdata('sc_msg'))): ?>
    <div class="alert alert-success">
        <?=$this->session->flashdata('sc_msg');?>
    </div>
<?php endif; ?>