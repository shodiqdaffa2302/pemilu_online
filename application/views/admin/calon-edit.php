<?php $this->load->view('admin/partial/header'); ?>

	<div class="row">
		<div class="col-md-12">
		  <form action="<?=base_url('admin/calon/update/'.$data['artikel']->id);?>" method="POST" enctype="multipart/form-data" >
			<div class="form-group">
			  <label>Visi</label>
			  <textarea class="form-control" name="visi" rows="8" cols="40"><?=$data['artikel']->visi;?></textarea>
			</div>
			
			<div class="form-group">
				<label>Misi</label>
				<textarea class="form-control" name="misi" rows="8" cols="40"><?=$data['artikel']->misi;?></textarea>
			</div>
			
			<div class="form-group">
				<label>Warna</label>
				<script src="<?=base_url('assets/js/jscolor/jscolor.min.js');?>" type="text/javascript" charset="utf-8"></script>
				<input type="text" class="form-control jscolor" name="warna" rows="8" cols="40" value="<?=$data['artikel']->warna;?>" />
			</div>		
			
			<div class="row">
			  <div class="col-md-6">
				<h4>Ketua</h4>
				<br />
				
				<div class="form-group">
					<label>Foto</label>
					<br>
					<img src="<?=base_url('img/'.$data['artikel']->calon[0]->gambar);?>" style="width:100px; height:100px" />
					<br>
					<input class="form-control" type="file" name="foto_1"  />
				</div>
				
				<div class="form-group">
				  	<label>Nama</label>
				  	<input class="form-control" type="text" name="nama_calon_1" value="<?=$data['artikel']->calon[0]->nama_calon;?>" accept="image/*" />
				</div>
			  </div>
			  
			  <div class="col-md-6">
				<h4>Wakil</h4>
				<br />
				
				<div class="form-group">
					<label>Foto</label>
					<br>
					<img src="<?=base_url('img/'.$data['artikel']->calon[1]->gambar);?>" style="width:100px; height:100px" />
					<br>
					<input class="form-control" type="file" name="foto_2"  />
				</div>
				<div class="form-group">
				  	<label>Nama</label>
				  	<input class="form-control" type="text" name="nama_calon_2" value="<?=$data['artikel']->calon[1]->nama_calon;?>"/>
				</div>
			  </div>
			</div>
			
			<br />
			<div class="form-group">
			  <button class="btn btn-success">Simpan</button>
			</div>
			<div class="form-group">
			  <input type="reset" class="btn btn-danger" value="Batal">
		  	</div>
		  	<div class="form-group">
			  <a href="<?=base_url('admin/calon')?>" class="btn btn-warning">Kembali</a>
		  	</div>
		  </form>
		</div>
	</div>

<?php $this->load->view('admin/partial/footer'); ?>