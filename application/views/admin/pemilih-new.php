<?php $this->load->view('admin/partial/header'); ?>

<div class="row">
    <div class="col-md-12">
        <form action="<?=base_url('admin/pemilih/create');?>" method="POST">
            <div class="form-group">
                <label>NIS *</label>
                <input type="number" name="nis" class="form-control" value="<?php echo set_value('nis');?>" />
            </div>
            <div class="form-group">
                <label>Password *</label>
				<input type="password" name="password" class="form-control" value="<?php echo set_value('password'); ?>">
            </div>
            <div class="form-group">
                <label>Nama *</label>
				<input type="text" name="nama" class="form-control" value="<?php echo set_value('nama'); ?>">
            </div>
            <div class="form-group">
                <label>Kelas *</label>
				<input type="text" name="kelas" class="form-control" value="<?php echo set_value('kelas'); ?>">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-success">Simpan</button>
            </div>

        </form>
    </div>
</div>

<?php $this->load->view('admin/partial/footer'); ?>