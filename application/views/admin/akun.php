<?php $this->load->view('admin/partial/header'); ?>
<a href="<?=base_url('admin/akun/add');?>" class="btn btn-success">Tambah Pemilih</a>
<br>
<br>
<table class="table table-hover">
    <thead>
        <th>Username</th>
        <th>Nama</th>
    </thead>
    <tbody>
    <?php foreach ($akun as $key => $value): ?>
        <tr>
            <td><?=$value->username?></td>
            <td><?=$value->nama?></td>
            <td>
                <a href="<?=base_url('admin/akun/edit/'.$value->username)?>" class="btn btn-info btn-xs">Edit</a> 
                <a href="<?=base_url('admin/akun/delete/'.$value->username)?>" class="btn btn-danger btn-xs">Hapus</a>
            </td>
        </tr>
    <?php endforeach ?>
    </tbody>
</table>
<?php $this->load->view('admin/partial/footer'); ?>