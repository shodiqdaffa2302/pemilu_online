<?php $this->load->view('admin/partial/header'); ?>

<div class="row">
    <div class="col-md-12">
        <form action="<?=base_url('admin/akun/create');?>" method="POST">
            <div class="form-group">
                <label>Username *</label>
                <input type="text" name="username" class="form-control" value="<?php echo set_value('username');?>" />
            </div>
            <div class="form-group">
                <label>Password *</label>
				<input type="password" name="password" class="form-control" value="<?php echo set_value('password'); ?>">
            </div>
            <div class="form-group">
                <label>Nama *</label>
				<input type="text" name="nama" class="form-control" value="<?php echo set_value('nama'); ?>">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-success">Simpan</button>
            </div>

        </form>
    </div>
</div>

<?php $this->load->view('admin/partial/footer'); ?>