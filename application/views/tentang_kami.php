<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Tentang Kami</title>

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/font-awesome/css/font-awesome.css') ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/style.css') ?>">

</head>
<body class="tentang beranda">
	<div class="nav">
		<nav class="navbar navbar-fixed-top">
	        <div class="container">
	        	<div class="navbar-header">
	        		<button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#nav">
		        		<span class="icon-bar"></span>
		        		<span class="icon-bar"></span>
		        		<span class="icon-bar"></span>
	        		</button>
	        	</div>
	        	<div class="collapse navbar-collapse" id="nav">
		            <ul class="nav navbar-nav">
		            	<li><a href=".">Beranda</a></li>
		                <li><a href="<?php echo base_url('artikel') ?>">Artikel</a></li>
		                <li><a href="<?php echo base_url('pemilu') ?>">Pemilu Online</a></li>
		                <li><a href="<?php echo base_url('forum') ?>">Forum</a></li>
		                <li><a href="<?php echo base_url('tentang'); ?>">Tentang Kami</a></li>
		            </ul>
		            <ul class="nav navbar-nav navbar-right">
		            	<li><a href=""><span class="glyphicon glyphicon-log-in"></span>  Login</a></li>
		            </ul>
	        	</div>
	        </div>
	    </nav>
	</div>
	<div class="container all">
		<div class="col-md-8 col-md-push-2">
			<div class="judul-halaman">
				<h2>Kami</h2>
			</div>
			<div class="row">
				<div class="isi">
					<div class="col-md-3">
						<img src="<?php echo base_url('assets/img/abi.jpg') ?>" alt="" class="profile">
					</div>
					<div class="col-md-9">
						<div class="row">
							<div class="col-md-6 desc">
								<h4 class="name">Muhammad Rizki</h4>
								<h5>Anggota Struggle Team</h5>
								<h6>Backend Developer</h6>
								<h4 class="no-telp">0896651243</h4>
								<a href="#">rizkumuhammad475@gmail.com</a>
							</div>
							<div class="col-md-6 sosmed">
								<a href="">
									<img src="<?php echo base_url('assets/img/ig.png') ?>" alt="">
								</a>
								<a href="">
									<img src="<?php echo base_url('assets/img/fb.png') ?>" alt="">
								</a>
								<a href="">
									<img src="<?php echo base_url('assets/img/tlg.png') ?>" alt="" class="tlg">
								</a>
							</div>
						</div>
						<div class="row-content">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad beatae non, hic laborum rem dicta ut fugiat fugit in nobis. Necessitatibus nihil veniam totam ducimus facilis. Fugit ab voluptates impedit.</p>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vel reprehenderit corporis incidunt libero nihil impedit temporibus explicabo, veniam dolores numquam a sit eum, quod ex? Dolores facilis, vitae vero tempora!</p>
						</div>
					</div>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="isi">
					<div class="col-md-3">
						<img src="<?php echo base_url('assets/img/rafif.jpg') ?>" alt="" class="profile">
					</div>
					<div class="col-md-9">
						<div class="row">
							<div class="col-md-6 desc">
								<h4 class="name">Shodiq Daffa Andrian</h4>
								<h5>Ketua Struggle Team</h5>
								<h6>Backend Developer & Stack Developer</h6>
								<h4 class="no-telp">08233123122</h4>
								<a href="#">shodiqdaffa2302@gmail.com</a>
							</div>
							<div class="col-md-6 sosmed">
								<a href="">
									<img src="<?php echo base_url('assets/img/ig.png') ?>" alt="">
								</a>
								<a href="">
									<img src="<?php echo base_url('assets/img/fb.png') ?>" alt="">
								</a>
								<a href="">
									<img src="<?php echo base_url('assets/img/tlg.png') ?>" alt="" class="tlg">
								</a>
							</div>
						</div>
						<div class="row-content">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad beatae non, hic laborum rem dicta ut fugiat fugit in nobis. Necessitatibus nihil veniam totam ducimus facilis. Fugit ab voluptates impedit.</p>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vel reprehenderit corporis incidunt libero nihil impedit temporibus explicabo, veniam dolores numquam a sit eum, quod ex? Dolores facilis, vitae vero tempora!</p>
						</div>
					</div>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="isi">
					<div class="col-md-3">
						<img src="<?php echo base_url('assets/img/profile.jpg') ?>" alt="" class="profile">
					</div>
					<div class="col-md-9">
						<div class="row">
							<div class="col-md-6 desc">
								<h4 class="name">Yeppy Mangun Puspitajudin</h4>
								<h5>Anggota Struggle Team</h5>
								<h6>Frontend Developer</h6>
								<h4 class="no-telp">0877-2898-0880</h4>
								<a href="#">yeppymp@gmail.com</a>
							</div>
							<div class="col-md-6 sosmed">
								<a href="">
									<img src="<?php echo base_url('assets/img/ig.png') ?>" alt="">
								</a>
								<a href="">
									<img src="<?php echo base_url('assets/img/fb.png') ?>" alt="">
								</a>
								<a href="">
									<img src="<?php echo base_url('assets/img/tlg.png') ?>" alt="" class="tlg">
								</a>
							</div>						</div>
						<div class="row-content">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad beatae non, hic laborum rem dicta ut fugiat fugit in nobis. Necessitatibus nihil veniam totam ducimus facilis. Fugit ab voluptates impedit.</p>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vel reprehenderit corporis incidunt libero nihil impedit temporibus explicabo, veniam dolores numquam a sit eum, quod ex? Dolores facilis, vitae vero tempora!</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="foot">
		<p>&copy; Struggle Team All rights Reserved</p>
	</div>
</body>
	<script type="text/javascript" src="<?php echo base_url('assets/jquery.min.js') ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js') ?>"></script>
</html>