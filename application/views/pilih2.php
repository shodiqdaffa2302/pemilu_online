<?php if ($this->session->flashdata('alert')): ?>
  <div class="alert alert-danger"><?=$this->session->flashdata('alert')?></div>
<?php endif ?>
<form action="<?=base_url('pemilu/kirim')?>" method="POST" >
  <?php $counter = 1; $x=1; ?>
  <div class="clearfix">
  <?php foreach ($data['calon'] as $calon): ?>
    <div class="calon-satu col-md-4">
      <h3><b>Paslon <?=$counter;?></b></h3>
      <?php foreach ($calon->calon as $cal): ?>
        <?=$cal->nama_calon;?>
        <?php if ($x==1) {
          echo " & ";
        } $x++;?>
      <?php endforeach ?>
      <?php $x=1; ?><br>
      <?php foreach ($calon->calon as $cal): ?>
        <img src="<?php echo base_url('img/'.$cal->gambar); ?>" alt="">        
      <?php endforeach ?>
          <input type="radio" name="pilih" value="<?=$calon->id?>">        
    </div>
    <?php $counter++; ?>
  <?php endforeach ?>
  </div>
          <input type="submit" class="btn btn-success btn-block" value="Ya" onclick="return confirm('Apakah anda yakin ?')">
</form>

