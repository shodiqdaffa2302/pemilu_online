<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
	<title>Home Page</title>

	<link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/style.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/font-awesome/css/font-awesome.css') ?>">
</head>
<body class="beranda">
	<div class="nav">
		<nav class="navbar navbar-fixed-top">
	        <div class="container">
	        	<div class="navbar-header">
	        		<button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#nav">
		        		<span class="icon-bar"></span>
		        		<span class="icon-bar"></span>
		        		<span class="icon-bar"></span>
	        		</button>
	        	</div>
	        	<div class="collapse navbar-collapse" id="nav">
		            <ul class="nav navbar-nav">
		            	<li><a href=".">Beranda</a></li>
		                <li><a href="#artikel">Artikel</a></li>
		                <li><a href="#pemilu">Pemilu Online</a></li>
		                <li><a href="#forum">Forum</a></li>
		                <li><a href="<?php echo base_url('tentang'); ?>">Tentang Kami</a></li>
		            </ul>
		            <ul class="nav navbar-nav navbar-right">
		            	<?php if ($this->session->userdata('credential2')): ?>
							<li><a href="<?=base_url('logout')?>">Logout</a></li>
		            	<?php else: ?>
			            	<li><a href="<?= base_url('login'); ?>">Login</a></li>		            		
		            	<?php endif ?>
		            </ul>
	        	</div>
	        </div>
	    </nav>
	</div>

	<div class="page">
		<div class="section-image ganjil">
			<div id="myCarousel" class="carousel slide" data-ride="carousel">

				<ol class="carousel-indicators">
					<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
					<li data-target="#myCarousel" data-slide-to="1"></li>
					<li data-target="#myCarousel" data-slide-to="2"></li>
				</ol>

				<div class="carousel-inner" role="listbox">
					<div class="item active">
						<img src="<?php echo base_url('assets/img/artikel.jpg') ?>" alt="Artikel">
					</div>

					<div class="item">
						<img src="<?php echo base_url('assets/img/pemilu2.jpg') ?>" alt="Pemilu">
					</div>

					<div class="item">
						<img src="<?php echo base_url('assets/img/forum.jpg') ?>" alt="Forum">
					</div>
				</div>

			</div>
		</div>

		<div class="carousel-button">
			<a href="#myCarousel" role="button" data-slide="prev">
				<i class="fa fa-chevron-left"></i>
			</a>
			<a href="#myCarousel" role="button" data-slide="next">
				<i class="fa fa-chevron-right"></i>
			</a>
		</div>
		
		<div class="section-artikel genap" id="artikel">
			<h1 class="title">ARTIKEL</h1>
			<div class="col-md-2 col-md-push-1">
				<img class="img img-circle" src="<?php echo base_url('assets/img/artikel.jpg'); ?>" alt="">
			</div>
			<div class="col-md-7 col-md-push-2">
				<p>
					Fitur ini akan menginformasikan kepada pengunjung tentang inovasi teknologi untuk pembelajaran indonesia yang lebih pintar.
					<br>
					Pengunjung dapat mengetahui tentang inovasi teknologi terbaru yang berkaitan dengan pembelajaran di Indonesia.
				</p>
				<a href="<?php echo base_url("artikel"); ?>">
					<input type="button" class="btn btn-default btn-md" id="buttonArtikel" value="Lihat Artikel">
				</a>
			</div>
		</div>
		
		<div class="section-pemilu ganjil" id="pemilu">
			<h1 class="title">Pemilu Online</h1>
			<div class="col-md-2 col-md-push-1">
				<img class="img img-circle" src="<?php echo base_url('assets/img/pemilu2.jpg'); ?>" alt="">
			</div>
			<div class="col-md-7 col-md-push-2">
				<p>
					Fitur ini dapat memudahkan kita dalam pemilihan Organisasi Siswa Intra Sekolah (OSIS) ,pemilihan Ketua Murid (KM) ,maupun pemilihan lainnya.
					<br>
					Kita juga dapat belajar bagaimana sistem Pemilihan Umum di dunia politik dan menggunakan hak suara kita dengan baik dan benar.
				</p>
				<a href="<?php echo base_url("pemilu") ?>">
					<input type="button" class="btn btn-default btn-md" id="buttonPemilu" value="Kunjungi Fitur">
				</a>
			</div>
		</div>

		<div class="section-forum genap" id="forum">
			<h1 class="title">FORUM</h1>
			<div class="row-satu">
				<div class="col-md-2 col-md-push-1">
					<img class="img img-circle" src="<?php echo base_url('assets/img/forum.jpg'); ?>" alt="">
				</div>
				<div class="col-md-7 col-md-push-2">
					<p>
						Forum adalah sebuah halaman yang dapat menampung komentar dan aspirasi semua pelajar.
						<br>
						Dalam forum juga pelajar dapat berbagi saran, berbagi pengetahuan, dan juga dapat bertukar pendapat dalam berbagai topik pembelajaran.
					</p>
					<a href="<?php echo base_url('forum'); ?>">
						<input type="button" class="btn btn-default btn-md" id="buttonArtikel" value="Kunjungi">
					</a>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 footer">
					Struggle LCW &copy; 2017
				</div>
			</div>
		</div>
	</div>
</body>
	<script src="<?php echo base_url('assets/jquery.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js') ?>"></script>
    <script type="text/javascript">
    	$(document).ready(function(){
			$(window).scroll(function() {
				if ($(this).scrollTop()>0){
				    $('.carousel-button').fadeOut();
				}
				else{
				  $('.carousel-button').fadeIn();
				}
			});
    	});
    </script>
</html>
