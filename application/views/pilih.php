<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    <title>Pemilihan Umum</title>
    <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/style.css') ?>">
  </head>
  <body class="pilih">
    <nav class="navbar navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#nav">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div class="collapse navbar-collapse" id="nav">
          <ul class="nav navbar-nav">
            <li><a href="<?php echo base_url('beranda') ?>">Beranda</a></li>
            <li><a href="<?php echo base_url('artikel') ?>">Artikel</a></li>
            <li><a href="<?php echo base_url('pemilu') ?>">Pemilu Online</a></li>
            <li><a href="<?php echo base_url('forum') ?>">Forum</a></li>
            <li><a href="<?php echo base_url('beranda/#tentang_kami') ?>">Tentang Kami</a></li>
          </ul>
        <ul class="nav navbar-nav navbar-right">
            <li>
                <a href="<?php echo base_url('logout') ?>">Logout</a>
            </li>
        </ul>
        </div>
      </div>
    </nav>
    <div class="container-side-bar">
        <div class="wrapper">
              <div class="side-bar">
                    <ul class="navbar">
                        <li class="menu-head">PEMILU ONLINE<a href="#" class="push_menu"><span class="glyphicon glyphicon-align-justify pull-right"></span></a>
                        </li>
                        <div class="menu">
                            <li>
                                <a  href="<?php echo base_url('pemilu') ?>">Cara Penggunaan</a>
                            </li>
                            <li><a href="<?php echo base_url('pemilu/profil') ?>">Profil Paslon</a></li>
                            <li>
                                <a href="<?php echo base_url('pemilu/pilih') ?>">Pilih</a>
                            </li>
                        </div>
                        
                    </ul>
              </div>   
              <div class="content">
                <?php $this->load->view($template); ?>
        </div>
    </div>

    <script type="text/javascript" src="<?php echo base_url('assets/jquery.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js') ?>"></script>
    <script type="text/javascript">
    $(document).ready(function(){
        $(".push_menu").click(function(){
             $(".wrapper").toggleClass("active");
        });
    });
    </script>
  </body>
</html>
