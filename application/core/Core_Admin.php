<?php

class Core_Admin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->checkAdminIsLogin();
        date_default_timezone_set('Asia/Jakarta');
    }

    public function checkAdminIsLogin()
    {
      $cred = $this->session->userdata('credential');

      if(empty($cred))
      {
          redirect('/');
      }
}
}