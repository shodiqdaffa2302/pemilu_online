<?php

class Core_Model extends CI_Model
{
    protected static $table;
    protected static $primaryKey;

    public function get()
    {
        $data                              = $this->db->get( self::$table )->result();

        return $data;
    }

    public function get_new()
    {
        $this->db->order_by(self::$primaryKey,'DESC');
        $data                              = $this->db->get( self::$table )->result();
        return $data;
    }
    public function show($id)
    {
        $data                              = $this->db->get_where( self::$table , array(
            self::$primaryKey              => $id
        ))->row();

        return $data; 
    }

    public function create($data = array())
    {
        $data                               = $this->db->insert( self::$table , $data );

        return $data;
    }

    public function update($id, $data = array())
    {
        $this->db->set( $data );
        $this->db->where( self::$primaryKey , $id );
        $data                               = $this->db->update( self::$table );

        return $data;
    }
    public function pagging($number,$offset){
        
        $this->db->order_by(self::$primaryKey,'DESC');
        $data                               = $this->db->get(self::$table,$number,$offset)->result();

        return $data;

    }
    public function destroy($id)
    {
         $data =  $this->db->delete( self::$table , array(
            self::$primaryKey => $id
        ));

        return $data;
    }

    public function count_rows(){
        $data = $this->db->get(self::$table) -> num_rows();

        return $data;
    }
}