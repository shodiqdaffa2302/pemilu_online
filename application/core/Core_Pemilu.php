<?php

class Core_Pemilu extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->checkPemilihIsLogin();
        date_default_timezone_set('Asia/Jakarta');
    }
    public function checkPemilihIsLogin()
    {
      $cred = $this->session->userdata('credential2');

      if(empty($cred))
      {
          redirect('login');
      }
    }
}